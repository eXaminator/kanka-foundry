enum KankaSettings {
    accessToken = 'access_token',
    campaign = 'campaign',
}

export default KankaSettings;
